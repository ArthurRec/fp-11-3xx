{-==== Tsimmerman Arthur, 11-301 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}

minPair' (x1,x2) (y1,y2) = if x2 <= y2 then (x1,x2) else (y1,y2)

min_func' [x] = x 
min_func' (x:xs) = minPair' x (min_func' xs) 

maxRatio' (x1,x2) (y1,y2) = if ((fromIntegral x1)/(fromIntegral x2)) >= ((fromIntegral y1)/(fromIntegral y2)) then (x1,x2) else (y1,y2) 

max_func' [x] = x 
max_func' (x:xs) = maxRatio' x (max_func' xs)

rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = if (snd (min_func' xs)) == (snd (max_func' xs)) then [max_func' xs] else xs

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)

data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}

getOrbiters :: [Load a] -> [Load a]
getOrbiters [] = []
getOrbiters ((Orbiter x):xs) = (Orbiter x):(getOrbiters xs)
getOrbiters ((Probe x):xs) = getOrbiters xs

orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo x):xs) = (orbiters xs) ++ (getOrbiters x)
orbiters ((Rocket _ x):xs) = (orbiters xs) ++ (orbiters x)

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier = map getOuner

getOuner :: Phrase -> String
getOuner phrase = case phrase of
        (Warp _) -> "Kirk"
        (BeamUp _) -> "Kirk"
        (IsDead _) -> "McCoy"
        (LiveLongAndProsper) -> "Spock"
        (Fascinating) -> "Spock"
