-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True


data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа
		  deriving (Eq, Show)

eval :: Term -> Int
eval (Const x) = x
eval (Add x y) = eval (x) + eval (y)
eval (Sub x y) = eval (x)  - eval (y)
eval (Mult x y) = eval (x) *  eval (y)


-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3

simplify :: Term -> Term
simplify (Const x) = Const x
simplify (Add a b) = Add (simplify a) (simplify b)
simplify (Sub a b) = Sub (simplify a) (simplify b)
simplify (Mult (Add a b) c) = simplify (Add (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
simplify (Mult (Sub a b) c) = simplify (Sub (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
simplify (Mult c (Add a b)) = simplify (Add (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
simplify (Mult c (Sub a b)) = simplify (Sub (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
simplify (Mult a b) = Mult (simplify a) (simplify b)