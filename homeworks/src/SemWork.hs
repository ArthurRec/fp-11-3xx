module SemWork where

import Data.Char

data SpecTerm = Var String
              | Lambda String SpecTerm
              | Apply SpecTerm SpecTerm
              deriving Eq

instance Show SpecTerm where
    show (Var v) = v
    show (Lambda v term) = "(\\" ++ v ++ " . " ++ show term ++ ")"
    show (Apply term1 term2) = show term1 ++ " " ++ show term2

-- Вычисление терма на один шаг
evaluate :: SpecTerm -> Maybe SpecTerm
evaluate expr = case expr of
    (Apply apply@(Apply term1 term2) term3) -> Just $ Apply evaluate_apply term3
        where (Just evaluate_apply) = evaluate apply
    (Apply term1 apply@(Apply term2 term3)) -> Just $ Apply term1 evaluate_apply
        where (Just evaluate_apply) = evaluate apply
    (Apply (Lambda v1 term1) l2) -> Just (changeVar v1 l2 term1)
    term -> Just term

-- замена переменной var на term в inSpecTerm для разных случаев
changeVar :: String -> SpecTerm -> SpecTerm -> SpecTerm
changeVar var term inSpecTerm = case inSpecTerm of
    Var v -> if v == var 
                    then term 
                    else Var v

    lamb@(Lambda v (Var v1)) -> if v /= v1 && v1 == var 
                                    then Lambda (new_val v term) term  
                                    else lamb 

    lamb@(Lambda v (Lambda v2 t)) -> 

        if v == var || v2 == var
            then lamb
        else if term == Var v
            then  Lambda (new_val v t) (Lambda v2 (changeVar var term 
                            (changeVar v (Var $ new_val v t) t )))
        else if term == Var v2 
            then Lambda v (Lambda (new_val v2 t) (changeVar var term 
                            (changeVar v2 (Var $ new_val v2 t) t )))
        else Lambda v (Lambda v2 (changeVar var term t))

    lamb@(Lambda v t@(Apply term1 term2)) -> 

        if v == var
            then lamb
            else if term == Var v 
                 then Lambda (new_val v t) (changeVar var term 
                            (changeVar v (Var $ new_val v t) (Apply term1 term2)))
                 else Lambda v (changeVar var term (Apply term1 term2))

    apply@(Apply term1 term2) -> Apply (changeVar var term term1) (changeVar var term term2)
    where new_val v t = (changeArgOnFreeVar (v : (getUsedVars t)) "a")
          
-- Получиение всех используемых переменных из term
getUsedVars :: SpecTerm -> [String]
getUsedVars term = case term of
    Lambda v t -> v : getUsedVars t
    Apply t1 t2 -> getUsedVars t1 ++ getUsedVars t2
    Var v -> [v]

-- замена аргумента на свободную переменную (не используемую букву), не входящуюю в список usedVars 
changeArgOnFreeVar :: [String] -> String -> String
changeArgOnFreeVar usedVars var = if var `elem` usedVars
                                    then changeArgOnFreeVar usedVars (nextVar var)
                                    else var
                                where nextVar [ch] = [chr (ord ch + 1)]

-- Вычисление на много шагов
evaluateN :: SpecTerm -> SpecTerm
evaluateN expr = case expr of
    (Apply apply@(Apply term1 term2) term3) -> evaluateN $ Apply (evaluateN apply) term3    
    (Apply term1 apply@(Apply term2 term3)) -> evaluateN $ Apply term1 (evaluateN apply)
    (Apply (Lambda v1 term1) l2) -> evaluateN $ changeVar v1 l2 term1
    term -> term