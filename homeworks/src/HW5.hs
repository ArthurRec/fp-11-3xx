-- 2016-04-05 / 2016-04-12

module HW5
       ( Parser (..)
       , dyckLanguage
       , Arith (..)
       , arith
       , Optional (..)
       ) where

{--==========  PARSER ==========-}

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar :: Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) -> Right (c, cs)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (x,r1) -> case parse (many p) r1 of
    Left _ -> Right ([x],r1)
    Right (xs,r2) -> Right (x:xs, r2)

matching :: (Char -> Bool) -> Parser Char
matching p = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (c:cs) | p c -> Right (c, cs)
         | otherwise -> Left $ "Char " ++ [c] ++
                        " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit
           -- read <$> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

{--========  END PARSER ========-}

-- ((())())
helpdyckf :: String -> String -> Bool
helpdyckf "" "" = True
helpdyckf "" _ = False
helpdyckf (a : as) "" = helpdyckf as [a]
helpdyckf (a : as) (b : ass)
  | a == '(' = helpdyckf as (a : b : ass)
  | a == ')' && b == '(' = helpdyckf as ass
  | a == ')' && b /= '(' = False

dyckLanguage :: Parser String
dyckLanguage =  Parser $ \s -> case helpdyckf s "" of
  True -> Right (s, "")
  False -> Left "Error"



data Arith = Plus Arith Arith
           | Minus Arith Arith
           | Mul Arith Arith
           | Number Int
           deriving (Eq,Show)

-- ((123+4321)*(321-3123))+(123+321)
{- Add (Mul (Add (Const 123) (Const 4321))
            (Sub (Const 321) (Const 3213)))
       (Add (Const 123) (Const 321))
-}
arith :: Parser Arith
arith = undefined
-- In progress -

-- Инстансы функтора и аппликативного функтора

data Optional a = NoParam
                | Param a
                deriving (Eq,Show)

instance Functor Optional where
  fmap f opt = case opt of
    Param a -> Param (f a)
    NoParam -> NoParam

instance Applicative Optional where
  pure a = Param a
  (<*>) pf pa = case pf of
    NoParam -> NoParam
    Param f -> case pa of
      Param a -> Param (f a)
      NoParam -> NoParam