-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ testGroup "HWs"
            [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ]
          ,
          testGroup "SemWork evaluate" [
                       testCase "evaluate (\\x . x) y == y" $ evaluate (Apply (Lambda "x" (Var "x")) (Var "y")) @?= Just (Var "y"),

                       testCase "evaluate (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                $ evaluate (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                                       (Lambda "z" (Var "z")))
                                    @?= Just (Lambda "y" (Apply (Lambda "z" (Var "z")) (Var "y"))),

                       testCase "evaluate (\\x . \\y . x) (\\x . y) == (\\a . (\\x . y))"
                                $ show (evaluate (Apply (Lambda  "x" $ Lambda  "y" $ Var "x") (Lambda "x" $ Var "y")))
                                    @?= "Just (\\a . (\\x . y))",

                       testCase "evaluate (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                $ show (evaluate (Lambda "x" (Apply (Lambda "x" (Var "x")) (Var "x"))))
                                    @?= "Just (\\x . (\\x . x) x)",

                       testCase "evaluate (\\x . \\y . x) y = (\\a . y)"
                                $ show (evaluate (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Var "y")))
                                    @?= "Just (\\a . y)",

                       testCase "evaluate (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)"
                                $ show (evaluate (Apply (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                                       (Lambda "z" (Var "z")))
                                                       (Lambda "u" (Lambda "v" (Var "v")))))
                                    @?= "Just (\\y . (\\z . z) y) (\\u . (\\v . v))",

                       testCase "evaluate (\\x . \\y . \\z . x y) y = (\\a . \\z . y a)"
                                $ show (evaluate (Apply (Lambda "x" (Lambda "y" (Lambda "z" (Apply (Var "x") (Var "y"))))) (Var "y")))
                                    @?= "Just (\\a . (\\z . y a))"
                 ],
          testGroup "SemWork evaluateN" [
                       testCase "evaluateN (\\x . x) y == y" $ evaluateN (Apply (Lambda "x" (Var "x")) (Var "y")) @?= Var "y",

                       testCase "evaluateN (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                $ evaluateN (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                                       (Lambda "z" (Var "z")))
                                    @?= Lambda "y" (Apply (Lambda "z" (Var "z")) (Var "y")),

                       testCase "evaluateN (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)"
                                $ evaluateN (Apply (Apply (Lambda "x"
                                                       (Lambda "y" (Apply (Var "x") (Var "y"))))
                                                       (Lambda "z" (Var "z")))
                                                       (Lambda "u" (Lambda "v" (Var "v"))))
                                    @?= Lambda "u" (Lambda "v" (Var "v")),

                       testCase "evaluateN (\\x . \\y . x) y = (\\a . y)"
                                $ show (evaluateN (Apply (Lambda "x" (Lambda "y" (Var "x"))) (Var "y")))
                                    @?= "(\\a . y)",

                       testCase "evaluateN (\\x . \\y . \\z . x y) y = (\\a . \\z . y a)"
                                $ show (evaluateN (Apply (Lambda "x" (Lambda "y" (Lambda "z" (Apply (Var "x") (Var "y"))))) (Var "y")))
                                    @?= "(\\a . (\\z . y a))",

                       testCase "evaluateN (\\x . \\a . x a) a = (\\b . a b)"
                                $ show (evaluateN (Apply (Lambda "x" (Lambda "a" (Apply (Var "x") (Var "a")))) (Var "a")))
                                    @?= "(\\b . a b)"
                 ]
            ]